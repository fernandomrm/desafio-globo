from django.core.urlresolvers import reverse
from rest_framework import status, test
from rest_framework.authtoken.models import Token

from .factories import MenuFactory, CustomUserFactory
from .models import Menu


class MenuTestCase(test.APITestCase):

    def setUp(self):
        self.user = CustomUserFactory()

    def test_list_all_menus(self):
        MenuFactory.create_batch(5)
        url = reverse('menus')
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 5)

    def test_get_menu_by_id(self):
        menu = MenuFactory()
        MenuFactory.create_batch(5, parent_menu=menu)
        url = reverse('menus', args=[menu.id])
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], menu.id)
        self.assertEqual(len(response.data['items']), 5)

    def test_menu_not_found(self):
        url = reverse('menus', args=[7])
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_dont_create_menu_without_attentication(self):
        url = reverse('menus')
        response = self.client.post(url, {'name': 'Menu'})

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create_root_menu(self):
        self.client.force_authenticate(user=self.user)
        url = reverse('menus')
        response = self.client.post(url, {'name': 'Menu'})
        menus = Menu.objects.get_menus()

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(menus.count(), 1)
        self.assertTrue(bool(menus[0].url))

    def test_create_submenu(self):
        self.client.force_authenticate(user=self.user)
        url = reverse('menus')
        menu = MenuFactory()
        response = self.client.post(url, {
            'name': 'SubMenu', 'parent_menu': menu.id, 'url': 'http://localhost:8001/submenu'
        })

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(menu.items.count(), 1)

    def test_dont_update_menu_without_attentication(self):
        menu = MenuFactory()
        url = reverse('menus', args=[menu.id])
        response = self.client.put(url, {'name': 'Menu', 'url':  'unneeded url'})

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_root_menu(self):
        self.client.force_authenticate(user=self.user)
        menu = MenuFactory()
        client_url = menu.url
        url = reverse('menus', args=[menu.id])
        response = self.client.put(url, {'name': 'Menu', 'url':  'unneeded url'})
        menu.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Menu.objects.count(), 1)
        self.assertTrue(menu.name == 'Menu' and menu.url == client_url)

    def test_update_submenu(self):
        self.client.force_authenticate(user=self.user)
        submenu = MenuFactory(parent_menu=MenuFactory())
        new_url = 'http://localhost:8001/new-url'
        url = reverse('menus', args=[submenu.id])
        response = self.client.put(url, {'name': 'SubMenu', 'url':  new_url})
        submenu.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Menu.objects.count(), 2)
        self.assertTrue(submenu.name == 'SubMenu' and submenu.url == new_url)

    def test_update_submenu_dont_change_parent_menu(self):
        self.client.force_authenticate(user=self.user)
        menu = MenuFactory()
        submenu = MenuFactory(parent_menu=menu)
        new_url = 'http://localhost:8001/new-url'
        url = reverse('menus', args=[submenu.id])
        response = self.client.put(url, {'name': 'SubMenu', 'url':  new_url, 'parent_menu': '7'})
        submenu.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Menu.objects.count(), 2)
        self.assertTrue(submenu.name == 'SubMenu' and submenu.url == new_url and submenu.parent_menu == menu)

    def test_dont_delete_menu_without_attentication(self):
        menu = MenuFactory()
        url = reverse('menus', args=[menu.id])
        response = self.client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_delete_menu(self):
        self.client.force_authenticate(user=self.user)
        menu = MenuFactory()
        url = reverse('menus', args=[menu.id])
        response = self.client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Menu.objects.count(), 0)

    def test_logout(self):
        Token.objects.create(user=self.user)
        self.client.force_authenticate(user=self.user)
        url = reverse('logout')
        response = self.client.post(url)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Token.objects.count(), 0)

    def test_get_menu_by_slug(self):
        menu = MenuFactory(name='Root menu')
        submenu = MenuFactory(parent_menu=menu)
        MenuFactory.create_batch(5, parent_menu=menu)
        MenuFactory.create_batch(5, parent_menu=submenu)
        url = reverse('menu', args=['root-menu'])
        response = self.client.get(url)

        items = response.data['items']
        max_items_submenu = max(map(lambda a: len(a['items']), items))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(items), 6)
        self.assertEqual(max_items_submenu, 5)

    def test_get_menu_by_slug_not_found(self):
        MenuFactory(name='Root menu')
        url = reverse('menu', args=['menu'])
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
