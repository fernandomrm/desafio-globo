import factory
from django.utils.text import slugify
from factory.django import DjangoModelFactory

from .models import Menu, CustomUser


class CustomUserFactory(DjangoModelFactory):
    class Meta:
        model = CustomUser

    email = factory.Sequence(lambda n: 'user{0}@mail.com'.format(n))
    password = factory.Faker('password')


class MenuFactory(factory.DjangoModelFactory):
    class Meta:
        model = Menu

    name = factory.Faker('name')
    url = factory.LazyAttribute(lambda a: 'http://localhost:8000/{0}'.format(slugify(a.name)))
