from rest_framework import serializers

from .models import Menu


class MenuSerializer(serializers.ModelSerializer):
    class Meta:
        model = Menu
        fields = ('id', 'name', 'url', 'parent_menu')


class ShallowMenuSerializer(serializers.ModelSerializer):
    items = MenuSerializer(many=True, read_only=True)

    def __init__(self, *args, **kwargs):
        super(ShallowMenuSerializer, self).__init__(*args, **kwargs)
        data = kwargs.get('data', {})
        if self.instance:
            self.fields['parent_menu'].read_only = True
            if self.instance.is_root:
                self.fields['url'].read_only = True
        else:
            if not data.get('parent_menu'):
                self.fields['url'].read_only = True

    class Meta:
        model = Menu
        fields = ('id', 'name', 'url', 'items', 'parent_menu')


class DeepMenuSerializer(serializers.ModelSerializer):

    def get_fields(self):
        fields = super(DeepMenuSerializer, self).get_fields()
        fields['items'] = DeepMenuSerializer(many=True)
        return fields

    class Meta:
        model = Menu
        fields = ('id', 'name', 'url', 'items')
