from django.http import Http404
from rest_framework import permissions, status
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Menu
from .serializers import MenuSerializer, ShallowMenuSerializer, DeepMenuSerializer


class MenuView(APIView):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def get(self, request, menu_id=None):
        if menu_id:
            try:
                menu = Menu.objects.get(id=menu_id)
                serializer = ShallowMenuSerializer(menu)
            except Menu.DoesNotExist:
                raise Http404
        else:
            menus = Menu.objects.get_menus()
            serializer = MenuSerializer(menus, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = ShallowMenuSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            menu = serializer.save()
            if menu.parent_menu:
                serializer = ShallowMenuSerializer(menu.parent_menu)
            else:
                serializer = MenuSerializer(Menu.objects.get_menus(), many=True)
            return Response(serializer.data, status=status.HTTP_201_CREATED)

    def put(self, request, menu_id):
        try:
            menu = Menu.objects.get(id=menu_id)
        except Menu.DoesNotExist:
            raise Http404
        serializer = ShallowMenuSerializer(menu, data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            if menu.parent_menu:
                serializer = ShallowMenuSerializer(menu.parent_menu)
            else:
                serializer = MenuSerializer(Menu.objects.get_menus(), many=True)
            return Response(serializer.data)

    def delete(self, request, menu_id):
        try:
            menu = Menu.objects.get(id=menu_id)
        except Menu.DoesNotExist:
            raise Http404
        parent_menu = menu.parent_menu
        menu.delete()
        if parent_menu:
            serializer = ShallowMenuSerializer(parent_menu)
        else:
            serializer = MenuSerializer(Menu.objects.get_menus(), many=True)
        return Response(serializer.data)


class LogoutView(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        request.user.auth_token.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ClientMenuView(APIView):

    def get(self, request, slug):
        try:
            menu = Menu.objects.get_by_slug(slug)
        except Menu.DoesNotExist:
            raise Http404
        serializer = DeepMenuSerializer(menu)
        return Response(serializer.data)
