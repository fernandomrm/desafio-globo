from django.conf.urls import url
from rest_framework.authtoken import views

from .views import MenuView, LogoutView, ClientMenuView

urlpatterns = [
    url(r'^menus/$', MenuView.as_view(), name='menus'),
    url(r'^menus/(?P<menu_id>\d+)/$', MenuView.as_view(), name='menus'),
    url(r'^menu/(?P<slug>[\w-]+)/$', ClientMenuView.as_view(), name='menu'),
    url(r'^login/$', views.obtain_auth_token),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),
]
