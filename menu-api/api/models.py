from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.db import models
from django.utils.text import slugify


class CustomUserManager(BaseUserManager):
    def create_user(self, email, password):
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
        )

        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password):
        user = self.create_user(
            email,
            password=password,
        )
        user.is_admin = True
        user.is_staff = True
        user.save()
        return user


class CustomUser(AbstractBaseUser):
    email = models.EmailField(max_length=254, unique=True)

    objects = CustomUserManager()

    USERNAME_FIELD = 'email'


class MenuManager(models.Manager):
    def get_menus(self):
        return self.filter(parent_menu__isnull=True)

    def get_by_slug(self, slug):
        return self.get(parent_menu__isnull=True, url__endswith='/' + slug)


class Menu(models.Model):
    parent_menu = models.ForeignKey('self', null=True)
    name = models.CharField(max_length=254)
    url = models.URLField()

    @property
    def items(self):
        return self.menu_set.all()

    @property
    def is_root(self):
        return self.parent_menu is None

    def save(self, *args, **kwargs):
        if self.is_root and not self.id:
            self.url = 'http://localhost:8001/{0}'.format(slugify(self.name))
        super(Menu, self).save(*args, **kwargs)

    objects = MenuManager()
