.SILENT:

PYTHON=venv/bin/python3
PIP=venv/bin/pip
GUNICORN=venv/bin/gunicorn
FLAKE8=venv/bin/flake8

build-api:
	cd menu-api && virtualenv venv -p python3 --clear
	cd menu-api && ${PIP} install -U pip
	cd menu-api && ${PIP} install -r requirements.txt
	cd menu-api && ${PYTHON} manage.py migrate

build-web:
	cd menu-web && npm install

build-admin:
	cd menu-admin && npm install

create-user:
	cd menu-api && ${PYTHON} manage.py createsuperuser

setup: build-api build-web build-admin create-user

test-api:
	cd menu-api && ${PYTHON} manage.py test --settings=project.settings.test

test-admin:
	cd menu-admin && npm test

test: test-api test-admin

dev-api:
	cd menu-api && ${PYTHON} manage.py runserver

dev-admin:
	cd menu-admin && npm run dev-server

dev-web:
	cd menu-web && npm run dev-server

run-api:
	cd menu-api && ${GUNICORN} -w 4 --access-logfile=- project.wsgi:application

run-admin:
	cd menu-admin && npm run start

run-web:
	cd menu-web && npm run start

run-list: run-api run-admin run-web

run:
	$(MAKE) -j3 run-list

pep8:
	cd menu-api && ${FLAKE8} --max-line-length=119 --exclude=venv/,api/migrations . || true
