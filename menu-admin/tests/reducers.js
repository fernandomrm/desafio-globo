import expect from 'expect';

import {RECEIVE_MENUS, RECEIVE_MENU, LOGIN, LOGOUT} from '../src/actions';
import {menus, menu, user} from '../src/reducers';


const _menus = [
    {
        id: 1,
        name: 'Menu',
        url: 'http://localhost:8001/menu'
    }
];

const _menu = {
    id: 1,
    name: 'Menu',
    url: 'http://localhost:8001/menu',
    items: []
};
const _user = {token: 'token'};

describe('Reducers', () => {
    it('Reducer menus return initial state', () => {
        expect(menus(undefined, {})).toEqual([]);
    });

    it('Reducer menus change state with action RECEIVE_MENUS', () => {
        expect(
            menus([], {
                type: RECEIVE_MENUS,
                menus: _menus
            })
        ).toEqual(_menus);
    });

    it('Reducer menu return initial state', () => {
        expect(menu(undefined, {})).toEqual(null);
    });

    it('Reducer menu change state with action RECEIVE_MENU', () => {
        expect(
            menu(null, {
                type: RECEIVE_MENU,
                menu: _menu
            })
        ).toEqual(_menu);
    });

    it('User reducer return initial state', () => {
        expect(user(undefined, {})).toEqual({});
    });

    it('User reducer change state with LOGIN action', () => {
        expect(
            user({}, {
                type: LOGIN,
                user: _user
            })
        ).toEqual(_user);
    });

    it('User reducer change state with LOGOUT action', () => {
        expect(
            user(_user, {type: LOGOUT})
        ).toEqual({});
    });
});
