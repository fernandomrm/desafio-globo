import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import expect from 'expect';
import nock from 'nock';

import * as actions from '../src/actions';


const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

const _menus = [
    {
        id: 1,
        name: 'Menu',
        url: 'http://localhost:8001/menu'
    }
];

const _menu = {
    id: 1,
    name: 'Menu',
    url: 'http://localhost:8001/menu',
    items: []
};

describe('Actions', () => {
    afterEach(() => {
        nock.cleanAll()
    });

    it('Action request menus', () => {
        nock('http://localhost:8000/')
            .get('/menus/')
            .reply(200, _menus);

        const expectedActions = [{
            type: actions.RECEIVE_MENUS,
            menus: _menus
        }];
        const store = mockStore();

        return store.dispatch(actions.requestMenus())
              .then(() => {
                  expect(store.getActions()).toEqual(expectedActions)
              });
    });

    it('Action request menu by id', () => {
        nock('http://localhost:8000/')
            .get('/menus/1/')
            .reply(200, _menu);

        const expectedActions = [{
            type: actions.RECEIVE_MENU,
            menu: _menu
        }];
        const store = mockStore();

        return store.dispatch(actions.requestMenu(1))
              .then(() => {
                  expect(store.getActions()).toEqual(expectedActions)
              });
    });

    it('Action create menu', () => {
        nock('http://localhost:8000/')
            .post('/menus/')
            .reply(201, _menus);

        const expectedActions = [{
            type: actions.RECEIVE_MENUS,
            menus: _menus
        }];
        const store = mockStore();

        return store.dispatch(actions.saveMenu({name: 'Menu'}))
              .then(() => {
                  expect(store.getActions()).toEqual(expectedActions)
              });
    });

    it('Action create submenu', () => {
        nock('http://localhost:8000/')
            .post('/menus/')
            .reply(201, _menu);

        const expectedActions = [{
            type: actions.RECEIVE_MENU,
            menu: _menu
        }];
        const store = mockStore();

        return store.dispatch(actions.saveMenu({..._menu, id: null, parent_menu: 1}))
              .then(() => {
                  expect(store.getActions()).toEqual(expectedActions)
              });
    });

    it('Action update menu', () => {
        nock('http://localhost:8000/')
            .put('/menus/1/')
            .reply(200, _menus);

        const expectedActions = [{
            type: actions.RECEIVE_MENUS,
            menus: _menus
        }];
        const store = mockStore();

        return store.dispatch(actions.saveMenu(_menu))
              .then(() => {
                  expect(store.getActions()).toEqual(expectedActions)
              });
    });

    it('Action update submenu', () => {
        nock('http://localhost:8000/')
            .put('/menus/2/')
            .reply(200, _menu);

        const expectedActions = [{
            type: actions.RECEIVE_MENU,
            menu: _menu
        }];
        const store = mockStore();

        return store.dispatch(actions.saveMenu({..._menu, id: 2, parent_menu: 1}))
              .then(() => {
                  expect(store.getActions()).toEqual(expectedActions)
              });
    });

    it('Action delete menu', () => {
        nock('http://localhost:8000/')
            .delete('/menus/1/')
            .reply(200, []);

        const expectedActions = [{
            type: actions.RECEIVE_MENUS,
            menus: []
        }];
        const store = mockStore();

        return store.dispatch(actions.deleteMenu(_menu))
              .then(() => {
                  expect(store.getActions()).toEqual(expectedActions)
              });
    });

    it('Action delete submenu', () => {
        nock('http://localhost:8000/')
            .delete('/menus/2/')
            .reply(200, _menu);

        const expectedActions = [{
            type: actions.RECEIVE_MENU,
            menu: _menu
        }];
        const store = mockStore();

        return store.dispatch(actions.deleteMenu({..._menu, id: 2, parent_menu: 1}))
              .then(() => {
                  expect(store.getActions()).toEqual(expectedActions)
              });
    });

    it('Action login', () => {
        let _user = {token: 'token'};
        nock('http://localhost:8000/')
            .post('/login/')
            .reply(200, _user);

        const expectedActions = [{
            type: actions.LOGIN,
            user: _user
        }]
        const store = mockStore();

        return store.dispatch(actions.login({username: 'username', password: 'password'}))
            .then(() => {
                  expect(store.getActions()).toEqual(expectedActions)
            });
    });

    it('Action logout', () => {
        const expectedActions = [{
            type: actions.LOGOUT,
        }]
        nock('http://localhost:8000/')
            .post('/logout/')
            .reply(204);
        const store = mockStore();

        return store.dispatch(actions.logout())
            .then(() => {
                expect(store.getActions()).toEqual(expectedActions);
            });
    });
});
