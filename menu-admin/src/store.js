import thunk from 'redux-thunk';
import {createStore, combineReducers, applyMiddleware} from 'redux';
import {menus, menu, user} from './reducers';


const initialState = {
    user: JSON.parse(localStorage.getItem('user')) || {}
}
const rootReducer = combineReducers({menus, menu, user});
export default createStore(rootReducer, initialState, applyMiddleware(thunk));
