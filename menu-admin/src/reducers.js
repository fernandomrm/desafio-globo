import {RECEIVE_MENUS, RECEIVE_MENU, LOGIN, LOGOUT} from './actions';


export function menus(state = [], action) {
    switch (action.type) {
        case RECEIVE_MENUS:
            return action.menus;
        default:
            return state;
    }
}

export function menu(state = null, action) {
    switch (action.type) {
        case RECEIVE_MENU:
            return action.menu;
        default:
            return state;
    }
}

export function user(state = {}, action) {
    switch (action.type) {
        case LOGIN:
            return action.user;
        case LOGOUT:
            return {};
        default:
            return state
    }
}
