import fetch from 'isomorphic-fetch';

import store from './store';
import {LOGOUT} from './actions';


export default function request(url, method = 'GET', data = {}) {
    let token = store.getState().user.token;
    url = process.env.API_URL + url;

    const headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
    if (token) {
        headers['Authorization'] = 'Token ' + token;
    }

    const REQUEST = {
        method: method,
        headers: headers,
    }
    if (method != 'GET') {
        REQUEST['body'] = JSON.stringify(data);
    }

    return fetch(url, REQUEST)
        .then(response => {
            if (response.ok) {
                if (response.status == 204) {
                    return response.statusText;
                }
                return response.json();
            } else {
                if (response.status == 401) {
                    store.dispatch({type: LOGOUT});
                }
                let error = new Error(response.statusText);
                error.response = response;
                throw error;
            }
        });
}
