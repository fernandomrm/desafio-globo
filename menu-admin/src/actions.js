import request from './requests';


export const RECEIVE_MENUS = 'RECEIVE_MENUS';
export const RECEIVE_MENU = 'RECEIVE_MENU';
export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';

function receiveMenus(menus) {
    return {
        type: RECEIVE_MENUS,
        menus
    }
}

export function requestMenus() {
    return dispatch => {
        return request('/menus/')
            .then(menus => dispatch(receiveMenus(menus)));
    }
}

export function receiveMenu(menu) {
    return {
        type: RECEIVE_MENU,
        menu
    }
}

export function requestMenu(id) {
    return dispatch => {
        return request('/menus/' + id + '/')
            .then(menu => dispatch(receiveMenu(menu)));
    }
}

export function saveMenu(menu) {
    return dispatch => {
        let method = menu.id ? 'PUT' : 'POST';
        let url = '/menus/' + (menu.id ? menu.id + '/' : '');
        return request(url, method, menu)
            .then(result => {
                if (menu.parent_menu) {
                    dispatch(receiveMenu(result));
                } else {
                    dispatch(receiveMenus(result));
                }
            });
    }
}

export function deleteMenu(menu) {
    return dispatch => {
        return request('/menus/' + menu.id + '/', 'DELETE')
            .then(result => {
                if (menu.parent_menu) {
                    dispatch(receiveMenu(result));
                } else {
                    dispatch(receiveMenus(result));
                }
            });
    }
}

export function login(credentials) {
    return dispatch => {
        return request('/login/', 'POST', credentials)
            .then(user => {
                localStorage.setItem('user', JSON.stringify(user));
                dispatch({
                    type: LOGIN,
                    user
                });
            });
    }
}

export function logout() {
    return dispatch => {
        return request('/logout/', 'POST')
            .then(() => {
                localStorage.removeItem('user');
                dispatch({type: LOGOUT});
            });
    }
}
