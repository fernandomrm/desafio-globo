import React, {Component, PropTypes} from 'react';

import './stylesheets/menu-page.scss';
import store from './store';
import {requestMenus, requestMenu, receiveMenu, logout} from './actions';
import MenuItem from './menu-item';


export default class MenuPage extends Component {
    constructor() {
        super();
        this.goBack = this.goBack.bind(this);
    }

    componentWillMount() {
        store.dispatch(requestMenus());
    }

    goBack() {
        const {menu} = this.props;
        if (menu.parent_menu) {
            store.dispatch(requestMenu(menu.parent_menu));
        } else {
            store.dispatch(receiveMenu(null));
            store.dispatch(requestMenus());
        }
    }

    render() {
        const {menus, menu} = this.props;
        const title = menu ? menu.name : 'Menus';
        const items = menu ? menu.items : menus;
        const parent_menu = menu ? menu.id : null;

        return (
            <div className="menus-container">
                <nav className="nav">
                    {menu ? <button className="btn--edit" onClick={this.goBack}>Voltar</button> : <div />}
                    <h2>{title}</h2>
                    <button className="btn--edit" onClick={() => store.dispatch(logout())}>
                        Logout
                    </button>
                </nav>
                {items.map(menu =>
                    <MenuItem key={menu.id} menu={menu} />
                )}
                <MenuItem parent_menu={parent_menu} />
            </div>
        );
    }
}

MenuPage.propTypes = {
    menus: PropTypes.array.isRequired,
    menu: PropTypes.object
}
