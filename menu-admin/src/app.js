import React, {Component} from 'react';

import './stylesheets/app.scss';
import store from './store';
import MenuPage from './menu-page';
import LoginPage from './login-page';


export default class App extends Component {
    constructor() {
        super();
        this.state = store.getState();
        this.unsubscribe = store.subscribe(this.changeState.bind(this));
    }

    componentWillUnmount() {
        this.unsubscribe();
    }

    changeState() {
        this.setState(store.getState());
    }

    render() {
        const {menus, menu, user} = this.state;
        return (
            <div className="container">
                {user.token ? <MenuPage menus={menus} menu={menu} /> : <LoginPage />}
            </div>
        )
    }
}
