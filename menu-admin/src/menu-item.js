import React, {Component, PropTypes} from 'react';

import './stylesheets/menu-item.scss';
import store from './store';
import {requestMenu, saveMenu, deleteMenu} from './actions';


export default class MenuItem extends Component {
    constructor(props) {
        super();
        this.state = this.initialState(props);
        this.changeInput = this.changeInput.bind(this);
        this.submitMenu = this.submitMenu.bind(this);
    }

    initialState(props) {
        return {
            menu: {
                name: '', url: '', parent_menu: props.parent_menu, ...props.menu
            },
            message: {
                text: '', type: ''
            }
        };
    }

    componentWillReceiveProps(props) {
        this.state = this.initialState(props);
    }

    changeInput(e) {
        let menu = this.state.menu;
        menu[e.target.name] = e.target.value;
        this.setState({menu: menu});
    }

    submitMenu() {
        const menu = this.state.menu;
        if (menu.name && (menu.parent_menu ? menu.url : true)) {
            store.dispatch(saveMenu(menu))
                .then(() => {
                    const message = {text: 'Item salvo com sucesso', type: 'success'};
                    this.setState({message: message});
                })
                .catch(error => {
                    let message;
                    if (error.response.status == 400) {
                        message = {text: 'Insira uma URL válida', type: 'error'};
                    } else {
                        message = {text: 'Erro ao salvar o item', type: 'error'};
                    }
                    this.setState({message: message});
                });
        } else {
            const message = {text: 'Preencha todos os campos', type: 'error'};
            this.setState({message: message});
        }
    }

    renderURLInput() {
        const menu = this.state.menu;
        if (menu.parent_menu) {
            return (
                <input
                    name="url"
                    type="url"
                    value={menu.url}
                    onChange={this.changeInput}
                    placeholder="Insira uma URL"
                />
            );
        } else if (menu.url) {
            return <a href={menu.url} target="_blank">{menu.url}</a>;
        }
    }

    renderOptionalButtons() {
        const menu = this.state.menu;
        if (menu.id) {
            return [
                <button className="btn--edit" key={0} onClick={() => store.dispatch(requestMenu(menu.id))}>
                    Editar itens
                </button>,
                <button className="btn--remove" key={1} onClick={() => store.dispatch(deleteMenu(menu))}>
                    Remover
                </button>
            ];
        }
    }

    renderMessage() {
        const {text, type} = this.state.message;
        if (text) {
            return <div className={'message--' + type}>{text}</div>;
        }
    }

    render() {
        const menu = this.state.menu;
        return (
            <div>
                {this.renderMessage()}
                <div className="item-container">
                    <div className="item-container__inputs">
                        <div className="item-container">
                            <input
                                name="name"
                                type="text"
                                value={menu.name}
                                onChange={this.changeInput}
                                placeholder="Insira um nome"
                            />
                            {this.renderURLInput()}
                        </div>
                    </div>
                    <div className="item-container__buttons">
                        <div className="item-container">
                            <button className="btn--save" onClick={this.submitMenu}>
                                Salvar
                            </button>
                            {this.renderOptionalButtons()}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

MenuItem.propTypes = {
    menu: PropTypes.object,
    parent_menu: PropTypes.number
}
