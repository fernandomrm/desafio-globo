import React, {Component} from 'react';

import './stylesheets/login-page.scss';
import store from './store';
import {login} from './actions';


export default class LoginPage extends Component {
    constructor() {
        super();
        this.state = {
            username: '',
            password: '',
            errorMessage: ''
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault();
        const {username, password} = this.state;
        if (username && password) {
            store.dispatch(login({username, password}))
                .catch(error => {
                    let errorMessage = error.message;
                    if (error.response.status == 400) {
                        errorMessage = 'E-mail ou senha incorreto';
                    }
                    this.setState({errorMessage: errorMessage});
                });
        } else {
            this.setState({errorMessage: 'Os campos e-mail e senha são obrigatórios'});
        }
    }

    handleChange(e) {
        this.setState({[e.target.name]: e.target.value});
    }

    renderErrorMessage() {
        const {errorMessage} = this.state;
        if (errorMessage) {
            return <div className="message--error">{this.state.errorMessage}</div>;
        }
    }

    render() {
        const {username, password} = this.state;
        return (
            <div className="page-container">
                <h2>Login</h2>
                <form onSubmit={this.handleSubmit}>
                    {this.renderErrorMessage()}
                    <input
                        type="text"
                        onChange={this.handleChange}
                        name="username"
                        value={username}
                        placeholder="E-mail"
                    />
                    <input
                        type="password"
                        onChange={this.handleChange}
                        name="password"
                        value={password}
                        placeholder="Senha"
                    />
                    <button className="btn--save" type="submit">Login</button>
                </form>
            </div>
        );
    }
}
