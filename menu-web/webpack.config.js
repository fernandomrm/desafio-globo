var webpack = require('webpack');

module.exports = function() {
    process.env.API_URL = process.env.API_URL || 'http://localhost:8000/api';

    var config = {
        entry: './src/index.js',
        resolve: {
            root: __dirname + '/src',
        },
        output: {
            path: './static',
            filename: 'bundle.js'
        },
        plugins : [
            new webpack.EnvironmentPlugin(['NODE_ENV', 'API_URL']),
        ],
        module: {
            loaders: [
                {
                    test: /\.js$/,
                    exclude: __dirname + '/node_modules',
                    loader: 'babel',
                    query: {
                        presets: ['es2015', 'react'],
                        plugins: ['transform-object-rest-spread']
                    }
                },
                {
                    test: /\.s?css$/,
                    exclude: __dirname + '/node_modules',
                    loaders: ['style', 'css', 'sass'],
                }
            ]
        }
    };

    if (process.env.NODE_ENV == 'production') {
        config.plugins.push(
            new webpack.optimize.UglifyJsPlugin({
                compress: {warnings: true}
            })
        )
    }
    else {
        config.devtool = 'source-map'
    }
    return config
}();
