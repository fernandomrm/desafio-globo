var express = require('express');
var reactViews = require('express-react-views');
var fetch = require('isomorphic-fetch');


server = express();

server.set('views', __dirname + '/src');
server.set('view engine', 'js');
server.engine('js', reactViews.createEngine());
server.use(express.static(__dirname + '/static'));

server.get('/:slug', function(req, res) {
    var url = 'http://localhost:8000/api/menu/' + req.params.slug;
    var REQUEST = {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    };
    fetch(url, REQUEST)
        .then(response => {
            if (response.ok) {
                return response.json();
            }
            res.send(response.statusText);
        }).then(json => {
            res.render('html', { menu: json });
        }).catch(e => {
            res.send('Error');
        });
});

server.listen(8001, () => {
    console.log('Web server listening on port 8001!');
});
