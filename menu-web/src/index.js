import React from 'react'
import ReactDOM from 'react-dom'
import App from './app';


ReactDOM.render(
    <App menu={MENU} />,
    document.getElementById('app')
)
