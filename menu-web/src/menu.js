import React, {Component, PropTypes} from 'react';

import './stylesheets/menu.scss';


export default class Menu extends Component {
    shouldComponentUpdate(nextProps, nextState) {
        return false;
    }

    renderItem(item) {
        const {widthChange, level} = this.props;
        const {name, url, items} = item;
        const overLevel = level + (items.length ? 1 : 0);

        return (
            <li key={name} className={'menu__item' + (items.length ? '' : '--final')}>
                <a
                    href={item.url}
                    className="menu__link"
                    onMouseOver={() => widthChange(overLevel)}
                    onMouseLeave={() => widthChange(level)}
                >
                    <span>{item.name}</span>
                </a>
                {items.length ? <Menu {...item} level={level + 1} widthChange={widthChange} /> : null}
            </li>
        );
    }

    render() {
        const {name, url, items, level, widthChange} = this.props;
        const root = (level == 1);
        const leaveLevel = level - (root ? 0 : 1);

        return (
            <ul
                className={'menu' + (root ? '--root' : '')}
                onMouseLeave={() => widthChange(leaveLevel)}
                onMouseEnter={() => widthChange(level)}
            >
                {!root ? <a className="menu__link--title">{name}</a> : null}
                {items.map(item => this.renderItem(item))}
            </ul>
        );
    }
}

Menu.defaultProps = {
    level: 1
}

Menu.propTypes = {
    name: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    items: PropTypes.array.isRequired,
    level: PropTypes.number,
    widthChange: PropTypes.func.isRequired
}
