var React = require('react');
var ReactDOMServer = require('react-dom/server');

class Html extends React.Component {
    render() {
        const initScript = "var MENU = " + JSON.stringify(this.props.menu);
        return (
            <html>
                <head>
                    <meta charset="utf-8" />
                    <meta name="robots" content="noindex nofollow" />
                    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui" />
                    <title>Desafio Globo.com</title>
                    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" />
                </head>
                <body>
                    <div id="app"/>
                    <script dangerouslySetInnerHTML={{__html: initScript}} />
                    <script src="bundle.js"></script>
                </body>
            </html>
        );
    }
}

module.exports = Html;
