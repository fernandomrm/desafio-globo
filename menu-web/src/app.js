import React, {Component, PropTypes} from 'react';

import './stylesheets/app.scss';
import Menu from './menu';


export default class App extends Component {
    constructor() {
        super();
        this.state = {
            width: '200px'
        };
        this.widthChange = this.widthChange.bind(this);
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextState.width != this.state.width;
    }

    widthChange(level) {
        this.setState({width: (200 * level) + 'px'});
    }

    render() {
        const props = {...this.props.menu, widthChange: this.widthChange};
        return (
            <header>
                <div className="dropdown">
                    <i className="fa fa-bars fa-2x dropdown__icon" aria-hidden="true"/>
                    <span className="dropdown__label">Menu</span>
                    <div className="dropdown__content" style={this.state}>
                        <Menu {...props} />
                    </div>
                </div>
            </header>
        );
    }
}

App.propTypes = {
    menu: PropTypes.object.isRequired
}
