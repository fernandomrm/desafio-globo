## Fernando Matos
**Globo.com: coding challenge**

====================
#### Considerações Gerais
Você deverá usar este repositório como o repo principal do projeto, i.e., todos os seus commits devem estar registrados aqui, pois queremos ver como você trabalha.

**Registre tudo**: testes que forem executados, ideias que gostaria de implementar se tivesse mais tempo (explique como você as resolveria, se houvesse tempo), decisões que forem tomadas e seus porquês, arquiteturas que forem testadas e os motivos de terem sido modificadas ou abandonadas. Crie um arquivo COMMENTS.md ou HISTORY.md no repositório para registrar essas reflexões e decisões.

#### Requisitos **OBRIGATÓRIOS**
1. Utilize qualquer linguagem e framework web para criar a aplicação.
2. Configurar a aplicação com o comando `make setup`.
3. Executar a aplicação com o comando `make run`.
4. O header e o menu devem ser **responsivos**.
5. Não utilizar framework CSS (Bootstrap, Foundation, etc).

Envie as instruções necessárias para rodar o projeto localmente, incluindo todas as dependências.
Assuma que estamos rodando sua aplicação de um Mac OS e/ou Linux.

#### BÔNUS
1. Não utilizar nenhum framework no client (Javascript puro).
2. Utilizar algum pré-processador de css (Sass, Less, Stylus, etc).
3. Utilizar algum automatizador de tarefa (Grunt, Gulp, NPM script, etc).

=====================
#### O Desafio: Header e Menu
Esse projeto consiste em duas partes, ADMIN e CLIENT.

##### ADMIN

Crie uma aplicação web onde seja possível cadastrar diversos menus.

A aplicação deve permitir:

1. Cadastrar um novo menu (Ao salvar o menu deve ser gerada uma url para a visualização do mesmo no client).
2. Editar um menu (A url do client não é editável).
3. Ver a lista de menus cadastrados (Nome e Url do client).
4. Excluir um menu.

O cadastro do menu se dá da seguinte forma:

1. Nome do menu.
2. Adição dos itens do menu.

Os itens de um menu devem conter:

1. Label.
2. Link/Url.

**Eles também precisam ser editáveis e deletáveis.**

**Importante**

1. Um item de menu pode ser "pai" de outros itens.
2. Seus "filhos" também podem possuir outros itens e assim sucessivamente.

##### CLIENT

Nessa aplicação, faça uma página que exiba o menu cadastrado. O layout **desktop** do header e do menu deve ser igual as imagens abaixo, abrindo os subitens no hover.

***Exemplo 1***

![Header e Menu 1](example1.png?raw=true)

***Exemplo 2***

![Header e Menu 2](example2.png?raw=true)

***Exemplo 3***

![Header e Menu 3](example3.png?raw=true)

Você pode escolher as tecnologias que quiser (mas não vale usar plugins prontos de Header e Menu). Avaliaremos tanto a parte client-side como a de backend.

===============================================
#### O que será avaliado na sua solução?

1. Formatação e estruturação do código.
2. Performance (client e server).
3. Segurança.
4. Testes automatizados.
5. Design da interface.

=============
#### Dicas

- Use ferramentas e bibliotecas open source.
- Documente as decisões e os porquês.
- Automatize o que for possível.
- Em caso de dúvidas, pergunte.