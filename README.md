# Desafio Globo.com

A aplicação consiste de três partes, API, ADMIN e WEB. Essa arquitetura foi escolhida devido a independência e
flexibilidade adquirida com a utilização de microserviços.

*API*: Implementado utilizando Python3 e Django REST framework. Para servir a aplicação, foi utilizado o Gunicorn.

*ADMIN*: Implementado utilizando React e Redux. O Webpack foi utilizado para realizar o build da aplicação e o NPM para
gerenciar as dependências. Para servir a aplicação, foi utilizado o http-server.

*WEB*: Implementado utilizando React. O Webpack foi utilizado para realizar o build da aplicação e o NPM para gerenciar
as dependências. Para servir a aplicação, foi utilizado o express-react-views.

A escolha do Python3 e Django REST framework, se deu em função do fornecimento de um ecosistema propício a criação de
APIs RESTful e da prática do TDD.

O React por sua vez, permite a criação de componentes reutilizáveis com um ciclo de vida bem definido e com toda a sua
lógica encapsulada, tornando o código mais semântico e de fácil entendimento. Além disso, o React possui mecanismos que
reduzem as operações no DOM, tornando a aplicação mais rápida e agradável para o usuário.

O Redux, apesar de simplista, torna a aplicação previsível e fácil de testar, devido aos seus três princípios:

- A aplicação possui uma única fonte de verdade
- O estado da aplicação é somente leitura
- As alterações são feitas com funções puras.

Essas tecnologias foram escolhidas com base em todas essas características dentre outras.

Em termos de abstração, foi utilizado apenas a entidade menu, ao contrário de menu e submenu. Assim é possível inferir
sobre a entidade menu para saber mais informções sem ter a necessidade de uma distinção nítida. Essa modelagem
possibilitou tornar o código mais reutilizável em todos os elementos da arquitetura.

Com relação à API, poderia ter sido adicionado um mecanismo de cache, porém, optei por não utilizar para não prejudicar
os testes.

Com relação à aplicação WEB, não conseguir iniciar o desenvolvimento do menu responsivo, devido ao tempo tomado com as demais
partes da arquitetura e com os eventuais contratempos.

# Setup do projeto

Dependências do projeto: Python 3.5 e Node v4.4.4

Clonar repositório `git@github.com:SelecaoGlobocom/fernando-matos.git`

Entrar no diretório do projeto criado `cd fernando-matos`

Instala dependências e construir ambiente `make setup`

Ao final do comando será solicitado e-mail e senha para a criação do usuário a ser utilizado pelo ADMIN.

# Executar projeto

Executar no terminal `make run`

- API: [localhost:8000](http://localhost:8000)
- WEB: [localhost:8000](http://localhost:8001)
- ADMIN: [localhost:8000](http://localhost:8002)


# Rodar testes

`make test`

# PEP8
Checar se o código está de acordo com a PEP8: `make pep8`

# Descrição da API

`POST /api/login`
```json
{
    "username": "username",
    "password": "password"
}
```
Cria e retorna o token do usuário.

`POST /api/logout`

Remove o token do usuário.

`GET /api/menus`

Lista todos os principais menus.

`GET /api/menus/<id>`

Retorna o menu `<id>`.

`POST /api/menus/`
```json
{
    "name": "Menu",
    "url": "http://localhot:8001/menu",
    "parent_menu": null
}
```
Cria um menu e retorna a lista de menus ou o menu no qual está contido.

`PUT /api/menus/<id>`
```json
{
    "name": "Menu",
    "url": "http://localhot:8001/menu",
    "parent_menu": null
}
```
Atualiza as informações do menu e retorna a lista de menus ou o menu no qual está contido.

`DELETE /api/menus/<id>`

Exlui o menu `<id>` e retorna a lista de menus ou o menu no qual estava contido.

`GET /api/menu/<slug>`

Retorna o menu com o `slug` informado.
